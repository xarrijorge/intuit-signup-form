# intuit-signup-form
Signup Form for Intuit. 

See temporal live demo link here - http://raw.githack.com/johnsonsirv/intuit-signup-form/dev/index.html

# Contributors
Victor Johnson - https://github.com/johnsonsirv

Xarri Jorge - https://github.com/xarrijorge

# Project Description
This project models the frontend design of the Intuit Signup Page here

- https://accounts.intuit.com/signup.html?offering_id=Intuit.ifs.mint&namespace_id=50000026&redirect_url=https%3A%2F%2Fmint.intuit.com%2Foverview.event%3Ftask%3DS#

Project was completed using HTML & CSS 3